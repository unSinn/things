
shaftheight = 14;
shaftInsideDiameter = 5;
ballsize = 10;
holesize = 2.5;



difference() {
    union(){
        difference() {
            translate([0,0,3]) cylinder(shaftheight+3,shaftInsideDiameter+2,ballsize+0.2, center=true);
            translate([0,0,shaftheight-1.5]) sphere(ballsize, center=true);
        }
        translate([0,0,shaftheight-1.5]) {
            union(){
                difference() {
                    sphere(ballsize, center=true);
                    sphere(ballsize-3, center=true);
                    translate([0,ballsize,0]) cube(ballsize*2, center=true);
                }
                translate([0,-ballsize/2-2.2,0]) rotate([90,90,0]) cylinder(2.5,holesize+4,holesize+4, center=true);
            }
        }
    }
    translate([0,0,shaftheight-1.5]) rotate([90,90,0]) cylinder(100,holesize,holesize, center=true);
    cylinder(shaftheight+3,shaftInsideDiameter,shaftInsideDiameter, center=true);
    translate([0,ballsize/2+2,shaftheight+1]) rotate([90,0,0]) cylinder(ballsize,ballsize,ballsize, center=true);
    translate([0,-2*ballsize+3,shaftheight]) cube(ballsize*2, center=true);
}

